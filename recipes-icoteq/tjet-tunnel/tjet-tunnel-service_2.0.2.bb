SUMMARY = "Icoteq TransferJet IP tunnel daemon service"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8e7157a2d5a4ccdb0d8c137396a4b04b"
DEPENDS = "toshiba-tjet-drivers"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRCREV="master"
SRC_URI = "git://git@support.icoteq.com/ICO-TTJ-500/tjet-tunnel-service.git;protocol=ssh \
           file://tunnel_client.sh \
           file://tunnel_server.sh"

FILES_${PN} += "/home/root \
                /home/root/*"

S = "${WORKDIR}/git"

inherit autotools

do_install() {
	install -d ${D}${bindir}
	install -m 0777 ${S}/src/tjetTunnelService ${D}${bindir}
	install -d ${D}/home/root
	install -m 0777 ${WORKDIR}/tunnel_client.sh ${D}/home/root
	install -m 0777 ${WORKDIR}/tunnel_server.sh ${D}/home/root
}
