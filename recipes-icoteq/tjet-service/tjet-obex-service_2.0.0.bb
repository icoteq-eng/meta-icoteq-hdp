SUMMARY = "Icoteq TransferJet OBEX daemon service"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8e7157a2d5a4ccdb0d8c137396a4b04b"
DEPENDS = "toshiba-tjet-sdk"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRCREV="master"
SRC_URI = "git://git@support.icoteq.com/ICO-TTJ-500/tjet-obex-service.git;protocol=ssh \
           file://flexible.sh \
           file://reactive.sh \
           file://setuptj.sh \
           file://load_module.sh \
           file://start_daemon.sh \
           file://dot_profile \
           file://gui.conf \
           file://rfcnf.ini \
           file://inittab"

FILES_${PN} += "/home/root \
                /home/root/*"

S = "${WORKDIR}/git"

inherit autotools

do_install() {
	install -d ${D}${bindir}
	install -m 0777 ${S}/src/tjetObexService ${D}${bindir}
	install -d ${D}/home/root
	install -m 0666 ${WORKDIR}/gui.conf ${D}/home/root
	install -m 0777 ${WORKDIR}/flexible.sh ${D}/home/root
	install -m 0777 ${WORKDIR}/reactive.sh ${D}/home/root
	install -m 0777 ${WORKDIR}/load_module.sh ${D}/home/root
	install -m 0777 ${WORKDIR}/setuptj.sh ${D}/home/root
	install -m 0777 ${WORKDIR}/start_daemon.sh ${D}/home/root
	install -m 0777 ${WORKDIR}/rfcnf.ini ${D}/home/root
	install -m 0777 ${WORKDIR}/dot_profile ${D}/home/root/.profile
	install -d ${D}/etc
	install -m 0777 ${WORKDIR}/inittab ${D}/etc
}
