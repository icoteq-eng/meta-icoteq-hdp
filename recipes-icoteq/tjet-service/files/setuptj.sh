#!/usr/bin/env bash

modules=`lsmod | grep tos | awk '{print $1}'`
for m in $modules; do echo rmmod $m; done

./load_module.sh fit
