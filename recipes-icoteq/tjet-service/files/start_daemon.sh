setup_gpio_input() {
  gpio_num=$1
  echo "$gpio_num" > /sys/class/gpio/export
  echo "in" > /sys/class/gpio/gpio$gpio_num/direction
}

read_gpio_value() {
  echo `cat /sys/class/gpio/gpio$gpio_num/value`
}

GPIO=78
setup_gpio_input $GPIO
isFlexible=$(read_gpio_value $GPIO)

cd /home/root
if [ "$isFlexible" == "0" ]; then
  ./reactive.sh
else
  ./flexible.sh
fi
