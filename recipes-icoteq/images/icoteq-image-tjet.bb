DESCRIPTION = "Freescale X11 image with TJET demo"

include recipes-fsl/images/fsl-image-x11.bb

IMAGE_INSTALL_append = "python-gst \
	python-setuptools \
	python-pycairo \
	python-pygtk \
	python-evdev \
	python-pytjet-icoteq \
	toshiba-tjet-drivers \
	toshiba-tjet-sdk \
	tjet-obex-service \
	tjet-tunnel-service"

export IMAGE_BASENAME = "icoteq-image-tjet"
