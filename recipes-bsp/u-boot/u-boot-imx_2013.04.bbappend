SRC_URI = "git://git@support.icoteq.com/ICO-TTJ-500/uboot-imx;protocol=ssh"
SRCREV = "icoteq-hdp-ttj500-v2.0.0"

# save UBOOT_CONFIG as separate names
do_deploy_append() {
    install ${S}/${UBOOT_BINARY} ${DEPLOYDIR}/${UBOOT_IMAGE}_${UBOOT_CONFIG}
}