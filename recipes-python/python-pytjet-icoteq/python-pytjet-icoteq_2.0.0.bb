SUMMARY = "Python framework for TransferJet OBEX applications"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

SRCREV="master"
SRC_URI = "git://git@support.icoteq.com/ICO-TTJ-500/pytjet-icoteq.git;protocol=ssh"

S = "${WORKDIR}/git"

inherit setuptools

RDEPENDS_${PN} = "\
    udisks \
    python-evdev \
    python-gst \
    python-pygtk \
    python-setuptools \
    python-pycairo \
"
