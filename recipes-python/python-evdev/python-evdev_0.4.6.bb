SUMMARY = "Python bindings for uinput and evdev"
SECTION = "devel/python"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://LICENSE;md5=387617d95ca7894d9850cdeb502aa5bd"
SRCNAME = "evdev"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI = "https://pypi.python.org/packages/source/e/${SRCNAME}/${SRCNAME}-${PV}.tar.gz \
           file://setup-py.diff"
SRC_URI[md5sum] = "7dd7b329b09bfb56f4a8d136a0bb51c9"
SRC_URI[sha256sum] = "4846e536e218d7df6434ae0425058174b11cffae55fb55c4b63222f647f67f21"

S = "${WORKDIR}/${SRCNAME}-${PV}"

inherit setuptools

RDEPENDS_${PN} = "\
    python-ctypes \
"
