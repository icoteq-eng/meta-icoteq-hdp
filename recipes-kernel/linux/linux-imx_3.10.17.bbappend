FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SUMMARY = "Linux real-time kernel based on linux-imx"
DESCRIPTION = "Linux kernel that is based on Freescale's linux-imx, \
with added real-time capabilities."

SRCBRANCH = "imx_3.10.17_1.0.0_ga"
SRCREV = "33597e348b2d60dd5c71890ef7b7d3d3fd6e4e97"
LOCALVERSION = "-1.0.2_ga"

SRC_URI = "\
        git://git.freescale.com/imx/linux-2.6-imx.git;branch=${SRCBRANCH} \
        https://www.kernel.org/pub/linux/kernel/projects/rt/3.10/older/patch-3.10.17-rt12.patch.bz2;name=rt-patch1 \
        file://0001-fix-build.patch \
        file://0002-fix-build-with-rt-enabled.patch \
        file://0003-no-split-ptlocks.patch \
        file://imx6qttj500_defconfig \
        file://ttj500_dts.patch \
        file://dtbs_make.patch \
        file://imx6qdl_dtsi.patch \
        file://wlcore.patch \
        file://ttj500_sdio.patch \
        file://tsc2007.patch \
"

SRC_URI[rt-patch1.md5sum] = "77a28c8b20b01f280dcd860e606a6edd"
SRC_URI[rt-patch1.sha256sum] = "ce219268f08eecccb39ff2b5be83657d53ca67cb1c6b81021494075197190351"

do_configure_append() {
        echo "" > ${S}/.config
        CONF_SED_SCRIPT=""

        kernel_conf_variable LOCALVERSION "\"${LOCALVERSION}\""
        kernel_conf_variable LOCALVERSION_AUTO y

        sed -e "${CONF_SED_SCRIPT}" < '${WORKDIR}/imx6qttj500_defconfig' >> '${S}/.config'

        if [ "${SCMVERSION}" = "y" ]; then
                # Add GIT revision to the local version
                head=`git rev-parse --verify --short HEAD 2> /dev/null`
                printf "%s%s" +g $head > ${S}/.scmversion
        fi
}
